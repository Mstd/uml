User Story :
-	En tant qu’acheteur je veux pouvoir consulter un catalogue produit
-	En tant qu’acheteur je veux pouvoir enregistrer un achat afin de s’authentifier
-	En tant qu’acheteur je veux pouvoir enregistrer un achat afin de saisir les informations de livraison 
-	En tant qu’acheteur je veux pouvoir enregistrer un achat afin de constituer un panier
-	En tant qu’acheteur je veux pouvoir enregistrer un achat afin d’enregistrer des règlements 
-	En tant que système bancaire je veux pouvoir enregistrer un achat afin de sélectionner un client
-	En tant que livreur je veux pouvoir préparer une livraison
-	En tant que technicien je veux pouvoir consulter une remarque
